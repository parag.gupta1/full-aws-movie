const mysql = require('promise-mysql');

const pool = mysql.createPool({
  host: 'localhost',
  database: 'film',
  user: 'root',
  password: '',
  connectionLimit: 10,
  multipleStatements: true,
});

module.exports = pool;

pool.query('SELECT 1', [], (err, result) => {
  if (err) {
    console.log('Oh Shit ! DB not connected');
    // handleError()
  } else console.log('DB Connection is ok');
});

pool.on('acquire', (connection) => {
  console.log(`Connection ${connection.threadId} acquired`);
});

pool.on('enqueue', () => {
  console.log('Waiting for available connection slot');
});

pool.on('release', (connection) => {
  console.log(`Connection ${connection.threadId} released `);
});
