function handleError(err, req, res, next) {
  // console.log(err);
  console.log(err.stack);

  res.status(err.status || 500);
  res.json({
    success: false,
    status: 'error',
    message: err.message,
  });
}
module.exports.handleError = handleError;
